package com.company;

/**
 * Una clase es un molde que permite simular un objeto o una entidad f[sica
 * o conceptual; define un tipo o patr[on de un grupo de objetos y los comportamientos
 * que tienen en comun.
 */

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

/**
 * La firma del metodo de la case es "public class ObjectExample"
 * El modificador de acceso es public -> Se puede acceder a esta clase desde la misma clase, hijos clase y paquete.
  */

/**
 * La palabra reservada para decir que es una clase en Java es Class
 */

public class ObjectExampleAnimales {

    //-------------------ATRIBUTOS---------------------------

    /**
     * Los atributos son las caracteristicas de los objetos y estan dados en sustantivos
     */

    /**
     * String es tipo de dato no primitivo lo que permite hacer uso de todos los comportamientos de la clase Object
     * Todos los atributos que no son primitivos empeizan con una Mayucula, todos los tipos de datos cuenta con una clase que los representa
     * Tienen metodos para hacer conversiones entre ellas.
     */

    // - Cadenas de caracteres, representa muchos char
    public String raza;
    // - Clase que representa el atributo primitivo double. Atributo con modificador de acceso "private" permiso de accesibilidad desde la clase.
    private Double temperatura;
    // - atributo primitivo para datos reales.
    private double altura;
    // - Clase que representa el atributo primitivo int. Atributo con modificador de acceso "default" permiso de accesibilidad desde la clase y el paquete.
    Integer anios;
    // - atributo primitivo para datos enteros.
    private int meses;
    // - Clase que representa el atributo primitivo boolean. Atributo con modificador de acceso  "private" permiso de accesibilidad desde la clase.
    private Boolean extinto;
    // - atributo primitivo para datos binarios y los unicos valores posibles son "true" y "false".
    protected boolean vivo;
    // - Clase que representa el atributo primitivo char. Atributo con modificador de acceso  "private" permiso de accesibilidad desde la clase.
    private Character tipoSangre;
    // - atributo para datos de un solo caracter.
    private char RH;

    /**
     * Metodo constructor, permite construir objetos de la clase, caracteristica: tiene el mismo nombre de la clase
     * parametros - inputs que se requieren para ejecutar un m[etodo
     * @param raza
     * @param temperatura
     * @param altura
     * @param anios
     * @param meses
     * @param extinto
     * @param vivo
     * @param tipoSangre
     * @param RH
     */


    public ObjectExampleAnimales(String raza, Double temperatura, double altura, Integer anios, int meses, Boolean extinto, boolean vivo, Character tipoSangre, char RH) {

        // this indica la clase en la que se esta
        // el this se puede usar para indicar los atributos que tiene la clase o sus metodos

        this.raza = raza;
        this.temperatura = temperatura;
        this.altura = altura;
        this.anios = anios;
        this.meses = meses;
        this.extinto = extinto;
        this.vivo = vivo;
        this.tipoSangre = tipoSangre;
        this.RH = RH;
    }

    /**
     *Constructor vacio.
     * Sobrecarga: es la acci[on de utilizar m[as de una vez el nombre de un metodo dentro de una misma clase siempre y cuando los parametros sean diferentes.
     * NOTA: el tipo de dato de los parametros debe ser difernete o su cantidad.
     * Se usa principalmente en constructores
     */
    public ObjectExampleAnimales(){

    }

    /**
     * Metodo constructor con diferentes parametros
     * Sobrecarga.
     * @param raza
     * @param altura
     * @param anios
     */
    public ObjectExampleAnimales(String raza,double altura, int anios){
        this.raza = raza;
        this.altura = altura;
        this.anios = anios;
    }

    /**
     * Metodo Getter es un metodo de acceso para obtener informaci[on de un atributo
     * palabra reservada para saber que devuelve informaci[on es return
     * el tipo de dato de que se espera despuesta debe ser el mismo tipo de dato que se tiene en la firma del metodo
     * @return raza
     */

    public String getRaza() {
        return raza;
    }

    /**
     * Metodo setter: metodo de acceso para modificar informaci[on,
     * palabra reservada para decir que no retorna nada, void.
     * recibe un parametro como input para cambiar valores
     * El tipo de dato del valor a cambiar debe de ser el mismo de la informaci[on del atributo
     * @param raza
     */

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public Double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(Double temperatura) {
        this.temperatura = temperatura;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public Integer getAnios() {
        return anios;
    }

    public void setAnios(Integer anios) {
        this.anios = anios;
    }

    public int getMeses() {
        return meses;
    }

    public void setMeses(int meses) {
        this.meses = meses;
    }

    public Boolean getExtinto() {
        return extinto;
    }

    public void setExtinto(Boolean extinto) {
        this.extinto = extinto;
    }

    public boolean isVivo() {
        return vivo;
    }

    public void setVivo(boolean vivo) {
        this.vivo = vivo;
    }

    public Character getTipoSangre() {
        return tipoSangre;
    }

    public void setTipoSangre(Character tipoSangre) {
        this.tipoSangre = tipoSangre;
    }

    public char getRH() {
        return RH;
    }

    public void setRH(char RH) {
        this.RH = RH;
    }

    public void hacerSonido(){

        //Variable local es una variable que solo tiene vida en el Scope o alcance del metodo en el que fue creada
        //Existen variables globales que pueden ser accesadas segun su modificador
        String sonido = "No se que animal soy";
        System.out.println(sonido);
    }

}
