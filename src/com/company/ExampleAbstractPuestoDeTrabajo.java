package com.company;



public abstract class ExampleAbstractPuestoDeTrabajo {

    private String ubicacion;
    private double salario;

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public abstract double hacerAumento();


}
