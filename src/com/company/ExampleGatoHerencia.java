package com.company;

/**
 *La Herencia es la acci[on de extender de una clase a otra toda condici[on con modificador de acceso diferente de privado
 * Con la palabra reservada final en la firma de una clase se define que ninguna otra clase podra extender de esta.
 * extends es la palabra reservada para realizar herencia entre clases y subclases
 */

public final class ExampleGatoHerencia extends ObjectExampleAnimales{

    public void hacerSonido(String sonido){

        sonido = "miau";
        System.out.println(sonido);
    }

    public void metodo(){
        System.out.println(super.getAltura());
        hacerSonido();
        super.hacerSonido();
    }
}
